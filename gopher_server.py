#
# Enviroment: python3.4
#
# Run: python gopher_server.py
#      telnet localhost 51432

import socket

host = ''
port = 51432

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((host, port))
s.listen(1)

print('Server is ruuning on port %d; press Ctrl-c to ternimate.' % port)

while 1:
    clientsock, clientaddr = s.accept()
    print(str(clientaddr) + 'sign in')
    clientfile = clientsock.makefile('rw')
    clientfile.write('Welcome, ' + str(clientaddr) + '\n')
    clientfile.write('Please enter a string: ')
    line = clientfile.readline().strip()
    clientfile.write('You entered %d characters.\n' % len(line))
    clientfile.close()
    clientsock.close()